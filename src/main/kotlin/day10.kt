fun main() {

    val input = getLinesOfFile("day10.txt")

    val asteroids: List<Point2D> = parseMap(input)

    val solutionA = asteroids.map { it.countSights(asteroids) }.maxOrNull()
    println(solutionA) // 256

    val monitoringStation: Point2D = asteroids.maxByOrNull { it.countSights(asteroids) }!!

    val twoHundredAsteroid = asteroids.filterNot { it == monitoringStation }
        .groupBy { monitoringStation.angleTo(it) }
        .mapValues { it.value.sortedBy { a -> monitoringStation.distanceTo(a) } }
        .toSortedMap()
        .values
        .flatMap { it.withIndex() }
        .sortedBy { it.index }
        .map { it.value }
        .drop(199)
        .first()

    println((twoHundredAsteroid.x * 100) + twoHundredAsteroid.y) // 1707


}

private fun Point2D.countSights(asteroids: List<Point2D>): Int {
    return asteroids.filterNot { it == this }
        .map { this.angleTo(it) }
        .distinct()
        .size
}

private fun parseMap(input: List<String>): List<Point2D> {
    return input.withIndex().flatMap { (y, row) ->
        row.withIndex()
            .filter { '#' == it.value }
            .map { Point2D(it.index, y) }
    }
}
