package model

import java.util.*
import kotlin.math.pow

sealed class InstructionD11(val nextInstructionOffset: Int) {

    companion object {
        operator fun invoke(pointer: Int, instructions: MutableList<Long>): InstructionD11 {
            return when (val operation = (instructions[pointer] % 100).toInt()) {
                1 -> Add
                2 -> Multiply
                3 -> Input
                4 -> Output
                5 -> JumpIfTrue
                6 -> JumpIfFalse
                7 -> LessThan
                8 -> Equals
                9 -> AdjustRelativeBase
                99 -> Halt
                else -> throw IllegalArgumentException("Unknown operation: $operation")
            }
        }
    }

    abstract fun execute(
        pointer: Int,
        relativeBase: Int,
        instructions: MutableList<Long>,
        currentColor: Int,
        output: LinkedList<Int>
    ): Pair<Int, Int>

    object Add : InstructionD11(4) {
        override fun execute(
            pointer: Int,
            relativeBase: Int,
            instructions: MutableList<Long>,
            currentColor: Int,
            output: LinkedList<Int>
        ): Pair<Int, Int> {
            val writeTo = instructions.outputParameter(3, pointer, relativeBase)
            val a = instructions.inputParameter(1, pointer, relativeBase)
            val b = instructions.inputParameter(2, pointer, relativeBase)
            instructions.setAndFill(writeTo.toInt(), a + b)
            return Pair(nextInstructionOffset, 0)
        }
    }

    object Multiply : InstructionD11(4) {
        override fun execute(
            pointer: Int,
            relativeBase: Int,
            instructions: MutableList<Long>,
            currentColor: Int,
            output: LinkedList<Int>
        ): Pair<Int, Int> {
            val writeTo = instructions.outputParameter(3, pointer, relativeBase)
            val a = instructions.inputParameter(1, pointer, relativeBase)
            val b = instructions.inputParameter(2, pointer, relativeBase)
            instructions.setAndFill(writeTo.toInt(), a * b)
            return Pair(nextInstructionOffset, 0)
        }
    }

    object Input : InstructionD11(2) {
        override fun execute(
            pointer: Int,
            relativeBase: Int,
            instructions: MutableList<Long>,
            currentColor: Int,
            output: LinkedList<Int>
        ): Pair<Int, Int> {
            val writeTo = instructions.outputParameter(1, pointer, relativeBase)
            instructions.setAndFill(writeTo.toInt(), currentColor.toLong())
            return Pair(nextInstructionOffset, 0)
        }
    }

    object Output : InstructionD11(2) {
        override fun execute(
            pointer: Int,
            relativeBase: Int,
            instructions: MutableList<Long>,
            currentColor: Int,
            output: LinkedList<Int>
        ): Pair<Int, Int> {
            val outputToSend = instructions.inputParameter(1, pointer, relativeBase)
            output.addLast(outputToSend.toInt())
            return Pair(nextInstructionOffset, 0)
        }
    }

    object JumpIfTrue : InstructionD11(3) {
        override fun execute(
            pointer: Int,
            relativeBase: Int,
            instructions: MutableList<Long>,
            currentColor: Int,
            output: LinkedList<Int>
        ): Pair<Int, Int> {
            val a = instructions.inputParameter(1, pointer, relativeBase)
            val b = instructions.inputParameter(2, pointer, relativeBase)
            val pointerOffset = if (a != 0L) b - pointer else nextInstructionOffset
            return Pair(pointerOffset.toInt(), 0)
        }
    }

    object JumpIfFalse : InstructionD11(3) {
        override fun execute(
            pointer: Int,
            relativeBase: Int,
            instructions: MutableList<Long>,
            currentColor: Int,
            output: LinkedList<Int>
        ): Pair<Int, Int> {
            val a = instructions.inputParameter(1, pointer, relativeBase)
            val b = instructions.inputParameter(2, pointer, relativeBase)
            val pointerOffset = if (a == 0L) b - pointer else nextInstructionOffset
            return Pair(pointerOffset.toInt(), 0)
        }
    }

    object LessThan : InstructionD11(4) {
        override fun execute(
            pointer: Int,
            relativeBase: Int,
            instructions: MutableList<Long>,
            currentColor: Int,
            output: LinkedList<Int>
        ): Pair<Int, Int> {
            val writeTo = instructions.outputParameter(3, pointer, relativeBase)
            val a = instructions.inputParameter(1, pointer, relativeBase)
            val b = instructions.inputParameter(2, pointer, relativeBase)
            val valueToWrite = if (a < b) 1L else 0L
            instructions.setAndFill(writeTo.toInt(), valueToWrite)
            return Pair(nextInstructionOffset, 0)
        }
    }

    object Equals : InstructionD11(4) {
        override fun execute(
            pointer: Int,
            relativeBase: Int,
            instructions: MutableList<Long>,
            currentColor: Int,
            output: LinkedList<Int>
        ): Pair<Int, Int> {
            val writeTo = instructions.outputParameter(3, pointer, relativeBase)
            val a = instructions.inputParameter(1, pointer, relativeBase)
            val b = instructions.inputParameter(2, pointer, relativeBase)
            val valueToWrite = if (a == b) 1L else 0L
            instructions.setAndFill(writeTo.toInt(), valueToWrite)
            return Pair(nextInstructionOffset, 0)
        }
    }

    object AdjustRelativeBase : InstructionD11(2) {
        override fun execute(
            pointer: Int,
            relativeBase: Int,
            instructions: MutableList<Long>,
            currentColor: Int,
            output: LinkedList<Int>
        ): Pair<Int, Int> {
            val a: Long = instructions.inputParameter(1, pointer, relativeBase)
            return Pair(nextInstructionOffset, a.toInt())
        }
    }

    object Halt : InstructionD11(1) {
        override fun execute(
            pointer: Int,
            relativeBase: Int,
            instructions: MutableList<Long>,
            currentColor: Int,
            output: LinkedList<Int>
        ): Pair<Int, Int> = Pair(0, 0)
    }

    fun MutableList<Long>.setAndFill(index: Int, value: Long) {
        fillWithZero(index)
        this[index] = value
    }

    private fun MutableList<Long>.getAndFill(index: Int): Long {
        fillWithZero(index)
        return this[index]
    }

    private fun MutableList<Long>.fillWithZero(index: Int) {
        if (index < 0) {
            throw IllegalStateException("Index $index is not valid")
        }

        while (index !in this.indices) {
            this.add(0)
        }
    }

    fun MutableList<Long>.inputParameter(paramNo: Int, pointer: Int, relativeBase: Int): Long {
        return getInputModeValue(getAndFill(pointer).toParameterMode(paramNo), pointer + paramNo, relativeBase)
    }

    fun MutableList<Long>.outputParameter(paramNo: Int, pointer: Int, relativeBase: Int): Long {
        return getOutputModeValue(getAndFill(pointer).toParameterMode(paramNo), pointer + paramNo, relativeBase)
    }

    private fun Long.toParameterMode(pos: Int): Int {
        return (this / (10.0.pow(pos + 1).toInt()) % 10).toInt()
    }

    private fun MutableList<Long>.getInputModeValue(mode: Int, at: Int, relativeBase: Int): Long {
        return when (mode) {
            0 -> getAndFill(getAndFill(at).toInt())
            1 -> getAndFill(at)
            2 -> getAndFill(getAndFill(at).toInt() + relativeBase)
            else -> throw IllegalArgumentException("Unknown Input mode: $mode")
        }
    }

    private fun MutableList<Long>.getOutputModeValue(mode: Int, at: Int, relativeBase: Int): Long {
        return when (mode) {
            2 -> getAndFill(at) + relativeBase
            else -> getAndFill(at)
        }
    }

}
