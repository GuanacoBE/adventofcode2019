package model

import Point2D
import drawTheMap
import java.util.LinkedList
import kotlin.math.min

class ProgramD15(private val instructions: MutableList<Long>) {

    val visited: MutableMap<Point2D, Pair<String, Int>> = mutableMapOf()
    private var currentPosition = Point2D.ORIGIN
    private var distFromSource = 0

    fun run() {
        visited[currentPosition] = Pair(".", distFromSource)
        runSuspending()
    }

    private fun isNew(point: Point2D): Boolean {
        return !visited.containsKey(point)
    }

    private fun runSuspending() {
        var instructionPointer = 0
        var relativeBase = 0
        val inputs = LinkedList<Int>()
        val outputs = LinkedList<Int>()

        do {

            if (visited.map { it.value.first }.count { it == "." } == 797) {
                println("THE END")
                return
            }

            val nextOp = InstructionD15(instructionPointer, instructions)

            if (nextOp is InstructionD15.Input) {
                drawTheMap()

                var nextAction = getNextAction()

                // We already visited the 4 position
                if (nextAction == -1) {
                    val backTrack = listOf(
                        Pair(currentPosition.down(), 1),
                        Pair(currentPosition.up(), 2),
                        Pair(currentPosition.left(), 3),
                        Pair(currentPosition.right(), 4)
                    )
                        .map { Pair(visited[it.first]!!, it.second) }
                        .filter { it.first.first == "." }
                        .minByOrNull { it.first.second }!!

                    nextAction = backTrack.second
                }

                if (nextAction == -1) {
                    throw IllegalStateException("We are stuck !!")
                }

                inputs.addLast(nextAction)
            }

            val result = nextOp.execute(instructionPointer, relativeBase, instructions, inputs, outputs)
            instructionPointer += result.first
            relativeBase += result.second

            if (nextOp is InstructionD15.Output) {
                val actionInput = inputs.pop()
                val status = outputs.pop()

                val newPosition = when (actionInput) {
                    1 -> currentPosition.down()
                    2 -> currentPosition.up()
                    3 -> currentPosition.left()
                    4 -> currentPosition.right()
                    else -> throw IllegalStateException("Unknown input $actionInput")
                }

                val draw = when (status) {
                    0 -> "#"
                    1 -> "."
                    2 -> "O"
                    else -> throw IllegalStateException("Unknown output $status")
                }

                if (status == 2) {
                    println("Found robot at distance from source ${distFromSource + 1}")
                }

                if (status == 1 || status == 2) {
                    if (visited.containsKey(newPosition)) {
                        distFromSource = min(distFromSource, visited[newPosition]!!.second)
                    } else {
                        distFromSource++
                    }
                    currentPosition = newPosition
                }

                if (!visited.containsKey(newPosition)) {
                    visited[newPosition] = Pair(draw, distFromSource)
                }
            }


        } while (nextOp !is InstructionD15.Halt)
    }

    private fun getNextAction(): Int {
        if (isNew(currentPosition.down())) {
            return 1
        }
        if (isNew(currentPosition.up())) {
            return 2
        }
        if (isNew(currentPosition.left())) {
            return 3
        }
        if (isNew(currentPosition.right())) {
            return 4
        }
        return -1
    }

    private fun drawTheMap() {
        val toDraw = visited.toMutableMap()
        toDraw[currentPosition] = Pair("D", -1)
        drawTheMap(toDraw)
    }

}
