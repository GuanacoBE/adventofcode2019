fun main(args: Array<String>) {
    //val rawInput = "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\n" +
    //        "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"

    val rawInput = {}.javaClass.getResource("day3.txt")

    val wires = rawInput.readText(Charsets.UTF_8)
        .split("\n")
        .map { it.split(",") }

    val wire1 = parseWire(wires[0])
    val wire2 = parseWire(wires[1])

    val intersections = wire1.intersect(wire2)

    val result = intersections.minOf { it.distanceTo(Point2D.ORIGIN) }

    println(result) // 232

}

fun parseWire(wire: List<String>): List<Point2D> {
    var current = Point2D.ORIGIN
    return wire.flatMap {
        val direction = it.first()
        val steps = it.drop(1).toInt()
        (0 until steps).map {
            val next = when (direction) {
                'U' -> current.up()
                'D' -> current.down()
                'L' -> current.left()
                'R' -> current.right()
                else -> throw IllegalStateException("Invalid direction: $direction")
            }
            current = next
            next
        }

    }
}