package model

import Point2D
import java.util.LinkedList

class ProgramD11(private val instructions: MutableList<Long>) {

    var currentPosition = Point2D.ORIGIN
    var currentDirection = Direction.NORTH
    val ship: MutableMap<Point2D, Int> = mutableMapOf(currentPosition to 1)

    fun run() {
        runSuspending()
    }

    private fun runSuspending() {
        var instructionPointer = 0
        var relativeBase = 0
        val output = LinkedList<Int>()

        var firstOutput = true

        do {
            val currentColor = ship.getOrDefault(currentPosition, 0)
            val nextOp = InstructionD11(instructionPointer, instructions)

            val result = nextOp.execute(instructionPointer, relativeBase, instructions, currentColor, output)
            instructionPointer += result.first
            relativeBase += result.second

            if (output.isNotEmpty()) {
                if (firstOutput) {
                    ship[currentPosition] = output.removeFirst()
                } else {
                    val direction = output.removeFirst()
                    val (nextDirection, nextPosition) = when (direction) {
                        0 -> currentDirection.turnLeftAndMove(currentPosition)
                        1 -> currentDirection.turnRightAndMove(currentPosition)
                        else -> throw IllegalStateException("Direction $direction unknown")
                    }
                    currentDirection = nextDirection
                    currentPosition = nextPosition
                }
                firstOutput = !firstOutput
            }

        } while (nextOp !is InstructionD11.Halt)
    }

}

enum class Direction {
    NORTH {
        override fun turnLeftAndMove(from: Point2D): Pair<Direction, Point2D> = Pair(WEST, from.left())
        override fun turnRightAndMove(from: Point2D): Pair<Direction, Point2D> = Pair(EAST, from.right())
    },
    SOUTH {
        override fun turnLeftAndMove(from: Point2D): Pair<Direction, Point2D> = Pair(EAST, from.right())
        override fun turnRightAndMove(from: Point2D): Pair<Direction, Point2D> = Pair(WEST, from.left())
    },
    WEST {
        override fun turnLeftAndMove(from: Point2D): Pair<Direction, Point2D> = Pair(SOUTH, from.up())
        override fun turnRightAndMove(from: Point2D): Pair<Direction, Point2D> = Pair(NORTH, from.down())
    },
    EAST {
        override fun turnLeftAndMove(from: Point2D): Pair<Direction, Point2D> = Pair(NORTH, from.down())
        override fun turnRightAndMove(from: Point2D): Pair<Direction, Point2D> = Pair(SOUTH, from.up())
    };

    abstract fun turnLeftAndMove(from: Point2D): Pair<Direction, Point2D>
    abstract fun turnRightAndMove(from: Point2D): Pair<Direction, Point2D>
}
