import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlin.math.pow

fun main() {
    val rawInput = arrayOf(
        3,
        8,
        1001,
        8,
        10,
        8,
        105,
        1,
        0,
        0,
        21,
        46,
        63,
        76,
        97,
        118,
        199,
        280,
        361,
        442,
        99999,
        3,
        9,
        102,
        4,
        9,
        9,
        101,
        2,
        9,
        9,
        1002,
        9,
        5,
        9,
        101,
        4,
        9,
        9,
        102,
        2,
        9,
        9,
        4,
        9,
        99,
        3,
        9,
        101,
        5,
        9,
        9,
        102,
        3,
        9,
        9,
        101,
        3,
        9,
        9,
        4,
        9,
        99,
        3,
        9,
        1001,
        9,
        2,
        9,
        102,
        3,
        9,
        9,
        4,
        9,
        99,
        3,
        9,
        1002,
        9,
        5,
        9,
        101,
        4,
        9,
        9,
        1002,
        9,
        3,
        9,
        101,
        2,
        9,
        9,
        4,
        9,
        99,
        3,
        9,
        1002,
        9,
        5,
        9,
        101,
        3,
        9,
        9,
        1002,
        9,
        5,
        9,
        1001,
        9,
        5,
        9,
        4,
        9,
        99,
        3,
        9,
        102,
        2,
        9,
        9,
        4,
        9,
        3,
        9,
        1002,
        9,
        2,
        9,
        4,
        9,
        3,
        9,
        1002,
        9,
        2,
        9,
        4,
        9,
        3,
        9,
        1001,
        9,
        1,
        9,
        4,
        9,
        3,
        9,
        101,
        1,
        9,
        9,
        4,
        9,
        3,
        9,
        1001,
        9,
        1,
        9,
        4,
        9,
        3,
        9,
        1002,
        9,
        2,
        9,
        4,
        9,
        3,
        9,
        1001,
        9,
        2,
        9,
        4,
        9,
        3,
        9,
        102,
        2,
        9,
        9,
        4,
        9,
        3,
        9,
        102,
        2,
        9,
        9,
        4,
        9,
        99,
        3,
        9,
        1002,
        9,
        2,
        9,
        4,
        9,
        3,
        9,
        101,
        2,
        9,
        9,
        4,
        9,
        3,
        9,
        1001,
        9,
        1,
        9,
        4,
        9,
        3,
        9,
        101,
        2,
        9,
        9,
        4,
        9,
        3,
        9,
        102,
        2,
        9,
        9,
        4,
        9,
        3,
        9,
        1002,
        9,
        2,
        9,
        4,
        9,
        3,
        9,
        1002,
        9,
        2,
        9,
        4,
        9,
        3,
        9,
        101,
        2,
        9,
        9,
        4,
        9,
        3,
        9,
        1001,
        9,
        1,
        9,
        4,
        9,
        3,
        9,
        102,
        2,
        9,
        9,
        4,
        9,
        99,
        3,
        9,
        102,
        2,
        9,
        9,
        4,
        9,
        3,
        9,
        102,
        2,
        9,
        9,
        4,
        9,
        3,
        9,
        102,
        2,
        9,
        9,
        4,
        9,
        3,
        9,
        1001,
        9,
        2,
        9,
        4,
        9,
        3,
        9,
        101,
        1,
        9,
        9,
        4,
        9,
        3,
        9,
        101,
        2,
        9,
        9,
        4,
        9,
        3,
        9,
        102,
        2,
        9,
        9,
        4,
        9,
        3,
        9,
        102,
        2,
        9,
        9,
        4,
        9,
        3,
        9,
        1001,
        9,
        2,
        9,
        4,
        9,
        3,
        9,
        101,
        1,
        9,
        9,
        4,
        9,
        99,
        3,
        9,
        1002,
        9,
        2,
        9,
        4,
        9,
        3,
        9,
        102,
        2,
        9,
        9,
        4,
        9,
        3,
        9,
        1001,
        9,
        2,
        9,
        4,
        9,
        3,
        9,
        101,
        1,
        9,
        9,
        4,
        9,
        3,
        9,
        1001,
        9,
        1,
        9,
        4,
        9,
        3,
        9,
        1001,
        9,
        2,
        9,
        4,
        9,
        3,
        9,
        102,
        2,
        9,
        9,
        4,
        9,
        3,
        9,
        101,
        1,
        9,
        9,
        4,
        9,
        3,
        9,
        1001,
        9,
        1,
        9,
        4,
        9,
        3,
        9,
        101,
        2,
        9,
        9,
        4,
        9,
        99,
        3,
        9,
        101,
        1,
        9,
        9,
        4,
        9,
        3,
        9,
        1002,
        9,
        2,
        9,
        4,
        9,
        3,
        9,
        102,
        2,
        9,
        9,
        4,
        9,
        3,
        9,
        1002,
        9,
        2,
        9,
        4,
        9,
        3,
        9,
        102,
        2,
        9,
        9,
        4,
        9,
        3,
        9,
        101,
        1,
        9,
        9,
        4,
        9,
        3,
        9,
        101,
        2,
        9,
        9,
        4,
        9,
        3,
        9,
        1001,
        9,
        2,
        9,
        4,
        9,
        3,
        9,
        102,
        2,
        9,
        9,
        4,
        9,
        3,
        9,
        101,
        2,
        9,
        9,
        4,
        9,
        99
    ).toIntArray()

    val testInput = arrayOf(
        3, 26, 1001, 26, -4, 26, 3, 27, 1002, 27, 2, 27, 1, 27, 26,
        27, 4, 27, 1001, 28, -1, 28, 1005, 28, 6, 99, 0, 0, 5
    ).toIntArray()

    val sequences = listOf(9, 8, 7, 6, 5).permutations()

    val highestSignal = runBlocking {
        sequences.map { runAmplified(it, rawInput) }.maxOrNull()
    }

    print(highestSignal) // 17956613

}

private suspend fun runAmplified(settings: List<Int>, instructions: IntArray): Int = coroutineScope {

    val a = ProgramV4(instructions.copyOf(), listOf(settings[0], 0).toChannel())
    val b = ProgramV4(instructions.copyOf(), a.output.andSend(settings[1]))
    val c = ProgramV4(instructions.copyOf(), b.output.andSend(settings[2]))
    val d = ProgramV4(instructions.copyOf(), c.output.andSend(settings[3]))
    val e = ProgramV4(instructions.copyOf(), d.output.andSend(settings[4]))
    val channelSpy = ChannelSpy(e.output, a.input)

    coroutineScope {
        launch { channelSpy.listen() }
        launch { a.runSuspending() }
        launch { b.runSuspending() }
        launch { c.runSuspending() }
        launch { d.runSuspending() }
        launch { e.runSuspending() }
    }
    channelSpy.spy.receive()
}

private suspend fun <T> Channel<T>.andSend(msg: T): Channel<T> {
    return this.also { send(msg) }
}

private class ChannelSpy<T>(
    private val input: Channel<T>,
    private val output: Channel<T>,
    val spy: Channel<T> = Channel(Channel.CONFLATED)
) {

    suspend fun listen() = coroutineScope {
        for (received in input) {
            spy.send(received)
            output.send(received)
        }
    }
}

private class ProgramV4(private val instructions: IntArray, val input: Channel<Int>) {

    val output: Channel<Int> = Channel(Channel.UNLIMITED)

    suspend fun runSuspending() {
        var instructionPointer = 0

        do {
            val nextOp = Instruction(instructionPointer, instructions)
            instructionPointer += nextOp.execute(instructionPointer, instructions, input, output)
        } while (nextOp !is Instruction.Halt)

        output.close()
    }

}

private sealed class Instruction(val nextInstructionOffset: Int) {

    companion object {
        operator fun invoke(pointer: Int, instructions: IntArray): Instruction {
            return when (val operation = instructions[pointer] % 100) {
                1 -> Add
                2 -> Multiply
                3 -> Input
                4 -> Output
                5 -> JumpIfTrue
                6 -> JumpIfFalse
                7 -> LessThan
                8 -> Equals
                99 -> Halt
                else -> throw IllegalArgumentException("Unknown operation: $operation")
            }
        }
    }

    abstract suspend fun execute(
        pointer: Int,
        instructions: IntArray,
        input: ReceiveChannel<Int>,
        output: Channel<Int>
    ): Int

    object Add : Instruction(4) {
        override suspend fun execute(
            pointer: Int,
            instructions: IntArray,
            input: ReceiveChannel<Int>,
            output: Channel<Int>
        ): Int {
            val writeTo = instructions[pointer + 3]
            instructions[writeTo] = instructions.param(1, pointer) + instructions.param(2, pointer)
            return nextInstructionOffset
        }
    }

    object Multiply : Instruction(4) {
        override suspend fun execute(
            pointer: Int,
            instructions: IntArray,
            input: ReceiveChannel<Int>,
            output: Channel<Int>
        ): Int {
            val writeTo = instructions[pointer + 3]
            instructions[writeTo] = instructions.param(1, pointer) * instructions.param(2, pointer)
            return nextInstructionOffset
        }
    }

    object Input : Instruction(2) {
        override suspend fun execute(
            pointer: Int,
            instructions: IntArray,
            input: ReceiveChannel<Int>,
            output: Channel<Int>
        ): Int {
            val writeTo = instructions[pointer + 1]
            instructions[writeTo] = input.receive()
            return nextInstructionOffset
        }
    }

    object Output : Instruction(2) {
        override suspend fun execute(
            pointer: Int,
            instructions: IntArray,
            input: ReceiveChannel<Int>,
            output: Channel<Int>
        ): Int {
            output.send(instructions.param(1, pointer))
            return nextInstructionOffset
        }
    }

    object JumpIfTrue : Instruction(3) {
        override suspend fun execute(
            pointer: Int,
            instructions: IntArray,
            input: ReceiveChannel<Int>,
            output: Channel<Int>
        ): Int {
            val a = instructions.param(1, pointer)
            val b = instructions.param(2, pointer)
            return if (a != 0) {
                b - pointer
            } else {
                nextInstructionOffset
            }
        }
    }

    object JumpIfFalse : Instruction(3) {
        override suspend fun execute(
            pointer: Int,
            instructions: IntArray,
            input: ReceiveChannel<Int>,
            output: Channel<Int>
        ): Int {
            val a = instructions.param(1, pointer)
            val b = instructions.param(2, pointer)
            return if (a == 0) {
                b - pointer
            } else {
                nextInstructionOffset
            }
        }
    }

    object LessThan : Instruction(4) {
        override suspend fun execute(
            pointer: Int,
            instructions: IntArray,
            input: ReceiveChannel<Int>,
            output: Channel<Int>
        ): Int {
            val writeTo = instructions[pointer + 3]
            instructions[writeTo] = if (instructions.param(1, pointer) < instructions.param(2, pointer)) {
                1
            } else {
                0
            }
            return nextInstructionOffset
        }
    }

    object Equals : Instruction(4) {
        override suspend fun execute(
            pointer: Int,
            instructions: IntArray,
            input: ReceiveChannel<Int>,
            output: Channel<Int>
        ): Int {
            val writeTo = instructions[pointer + 3]
            instructions[writeTo] = if (instructions.param(1, pointer) == instructions.param(2, pointer)) {
                1
            } else {
                0
            }
            return nextInstructionOffset
        }
    }

    object Halt : Instruction(1) {
        override suspend fun execute(
            pointer: Int,
            instructions: IntArray,
            input: ReceiveChannel<Int>,
            output: Channel<Int>
        ): Int = 0
    }

    fun IntArray.param(paramNo: Int, offset: Int): Int {
        return getModeValue(this[offset].toParameterMode(paramNo), offset + paramNo)
    }

    private fun Int.toParameterMode(pos: Int): Int {
        return this / (10.0.pow(pos + 1).toInt()) % 10
    }

    private fun IntArray.getModeValue(mode: Int, at: Int): Int {
        return when (mode) {
            0 -> this[this[at]]
            1 -> this[at]
            else -> throw IllegalArgumentException("Unknown mode: $mode")
        }
    }

}