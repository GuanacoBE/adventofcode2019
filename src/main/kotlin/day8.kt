fun main() {

    val rawInput = {}.javaClass.getResource("day8.txt").readText(Charsets.UTF_8)
    //val rawInput = "0222112222120000"

    val sizeWide = 25 // 25
    val sizeTall = 6 // 6

    val numberDigitsByLayer = sizeWide * sizeTall

    val layers: List<Layer> = buildLayers(rawInput, numberDigitsByLayer)

    val layerFewestZero: Layer = layers.minByOrNull { it.countDigit(0) }
        ?: throw IllegalStateException("No Layer with fewest 0")

    // A
    println(layerFewestZero.countDigit(1) * layerFewestZero.countDigit(2)) // 1224

    // B
    val finalImage = Image(25, 6, layers).toFinalImage()

    printImage(finalImage) // EBZUR

}

private fun buildLayers(input: String, numberDigitsByLayer: Int): List<Layer> {
    var rawInput = input
    val layers: MutableList<Layer> = mutableListOf()

    while (rawInput.isNotEmpty()) {
        val layer: List<Int> = rawInput.take(numberDigitsByLayer).toCharArray().map { it.digitToInt() }
        layers.add(Layer(layer.toIntArray()))
        rawInput = rawInput.drop(numberDigitsByLayer)
    }

    return layers
}

private class Image(private val sizeWide: Int, sizeTall: Int, private val layers: List<Layer>) {

    val image: Array<Array<Int?>> = Array(sizeTall) { arrayOfNulls(sizeWide) }

    fun toFinalImage(): Array<Array<Int?>> {
        for (i in image.indices) {
            for (j in image[i].indices) {
                for (layer in layers) {
                    val matrixLayer = layer.toMatrix(sizeWide)
                    if (matrixLayer[i][j] in 0..1) {
                        image[i][j] = matrixLayer[i][j]
                        break
                    }
                }

            }
        }
        return image
    }

}

private class Layer(private val input: IntArray) {
    fun countDigit(digitToCount: Int): Int = input.count { it == digitToCount }

    fun toMatrix(sizeWide: Int): List<List<Int>> = input.toList().chunked(sizeWide)
}
