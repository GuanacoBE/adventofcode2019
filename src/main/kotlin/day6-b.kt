fun main(args: Array<String>) {

    val rawInput = getLinesOfFile("day6.txt")

    var comLineNumber = -1
    var comLine = ""

    val distinctObject = mutableSetOf<String>()

    rawInput.forEachIndexed { index, line ->
        if (line.startsWith("COM)")) {
            comLineNumber = index
            comLine = line
        }
        val split = line.split(")")
        if (split[0] != "COM") {
            distinctObject.add(split[0])
        }
        if (split[1] != "COM") {
            distinctObject.add(split[1])
        }

    }
    rawInput.removeAt(comLineNumber)

    val orbits = rawInput.groupBy {
        val split = it.split(")")
        split[0]
    }.toMutableMap()

    val lineSplit = comLine.split(")")

    val children = orbits[lineSplit[1]]?.map {
        it.split(")")[0]
    }
    val comObject = OrbitObject2(lineSplit[0], children, null)
    orbits.remove(lineSplit[0])

    addChildrenToChildren(orbits, comObject)

    val youObject = findObject(comObject, "YOU")!!

    val path = mutableListOf<OrbitObject2>()

    val seen = mutableSetOf<String>()

    search(youObject, "SAN", path, seen)

    println(path.size - 3) // 439
}

private fun search(
    o: OrbitObject2,
    element: String,
    track: MutableList<OrbitObject2>,
    seen: MutableSet<String>
): Boolean {

    seen.add(o.name)

    if (o.name == element) {
        track.add(o)
        return true
    }

    if (o.parent != null) {
        if (!seen.contains(o.parent.name) && search(o.parent, element, track, seen)) {
            track.add(0, o)
            return true
        }
    }

    for (orbit in o.orbits) {
        if (!seen.contains(orbit.name) && search(orbit, element, track, seen)) {
            track.add(0, o)
            return true
        }
    }

    return false
}

private fun findObject(o: OrbitObject2, element: String): OrbitObject2? {
    if (o.name == element) {
        return o
    } else {
        o.orbits.forEach {
            val found = findObject(it, element)
            if (found != null) {
                return found
            }
        }
    }
    return null
}

private fun addChildrenToChildren(orbits: MutableMap<String, List<String>>, orbitObject: OrbitObject2) {
    orbitObject.orbits.forEach {
        it.addOrbit(orbits[it.name]?.map { o -> o.split(")")[1] }?.map { t -> OrbitObject2(t, it) })
        orbits.remove(it.name)
        addChildrenToChildren(orbits, it)
    }
}

private class OrbitObject2(val name: String, val parent: OrbitObject2?) {
    val orbits: MutableSet<OrbitObject2> = HashSet()

    constructor(name: String, childOrbit: List<String>?, parent: OrbitObject2?) : this(name, parent) {
        addOrbit(childOrbit?.map { OrbitObject2(it, this) })
    }

    fun addOrbit(o: List<OrbitObject2>?) {
        o?.let { orbits.addAll(it) }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as OrbitObject2

        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }

    override fun toString(): String {
        return "OrbitObject2(name='$name')"
    }

}
