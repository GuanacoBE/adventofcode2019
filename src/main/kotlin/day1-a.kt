fun main(args: Array<String>) {

    val input = {}.javaClass.getResource("day1.txt").readText(Charsets.UTF_8)
    val masses = input.split("\n")

    val result = masses
        .map { it.toInt() }
        .sumOf { computeFuel(it) }

    println(result) // 3347838
}

fun computeFuel(mass: Int): Int {
    return (mass / 3) - 2
}
