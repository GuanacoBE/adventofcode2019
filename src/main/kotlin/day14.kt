import kotlin.math.ceil
import kotlin.math.sign

fun main() {
    val input = getLinesOfFile("day14.txt")

    val reactions: Map<String, Pair<Long, List<Pair<Long, String>>>> = input.associate { parseReaction(it) }

    println(computeCost(reactions = reactions)) // 783895


    val part2Soluce =
        (0L..1000000000000L).binarySearch { 1000000000000L.compareTo(computeCost(reactions = reactions, amount = it)) }

    println(part2Soluce) // 1896688

}

private fun LongRange.binarySearch(func: (Long) -> Int): Long {
    var low = this.first
    var high = this.last
    while (low <= high) {
        val mid = (low + high) / 2
        when (func(mid).sign) {
            -1 -> high = mid - 1
            1 -> low = mid + 1
            0 -> return mid
        }
    }
    return low - 1
}

private fun computeCost(
    material: String = "FUEL",
    amount: Long = 1,
    inventory: MutableMap<String, Long> = mutableMapOf(),
    reactions: Map<String, Pair<Long, List<Pair<Long, String>>>>
): Long {

    if (material == "ORE") {
        return amount
    }

    val inventoryQuantity = inventory.getOrDefault(material, 0)

    val neededQuantity = if (inventoryQuantity > 0) {
        inventory[material] = (inventoryQuantity - amount).coerceAtLeast(0)
        amount - inventoryQuantity
    } else {
        amount
    }

    return if (neededQuantity > 0) {
        val recipe = reactions[material]!!

        val iterations = ceil(neededQuantity.toDouble() / recipe.first).toInt()

        val produced = recipe.first * iterations

        if (neededQuantity < produced) {
            inventory[material] = inventory.getOrDefault(material, 0) + produced - neededQuantity
        }

        recipe.second.sumOf { computeCost(it.second, it.first * iterations, inventory, reactions) }
    } else {
        0
    }

}

private fun parseReaction(reaction: String): Pair<String, Pair<Long, List<Pair<Long, String>>>> {
    val inputOutput = reaction.split(" => ")

    val inputs: List<Pair<Long, String>> = inputOutput.first().split(",")
        .map { it.trim() }
        .map {
            it.split(" ").let { s -> Pair(s.first().toLong(), s.last()) }
        }

    val (amount, code) = inputOutput.last().split(" ")

    return code to Pair(amount.toLong(), inputs)
}
