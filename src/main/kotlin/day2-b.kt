fun main(args: Array<String>) {
    val rawInput =
        "1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,10,1,19,2,19,6,23,2,13,23,27,1,9,27,31,2,31,9,35,1,6,35,39,2,10,39,43,1,5,43,47,1,5,47,51,2,51,6,55,2,10,55,59,1,59,9,63,2,13,63,67,1,10,67,71,1,71,5,75,1,75,6,79,1,10,79,83,1,5,83,87,1,5,87,91,2,91,6,95,2,6,95,99,2,10,99,103,1,103,5,107,1,2,107,111,1,6,111,0,99,2,14,0,0"

    val rawInputTest = "1,9,10,3,2,3,11,0,99,30,40,50"

    val input = rawInput.split(",")
        .map { it.toInt() }
        .toTypedArray()

    println(foundProgram(input)) // 6429
}

fun foundProgram(program: Array<Int>): Int? {
    for (i in 0..99) {
        for (j in 0..99) {
            program[1] = i
            program[2] = j

            val result = executeProgram(program.clone())
            if (result == 19690720) {
                return 100 * i + j
            }
        }
    }
    return null
}

fun executeProgram(program: Array<Int>): Int {
    var currentPosition = 0
    while (true) {
        val opCode = program[currentPosition]
        val positionLeft = program[currentPosition + 1]
        val positionRight = program[currentPosition + 2]
        val positionOutput = program[currentPosition + 3]
        when (opCode) {
            99 -> return program[0]
            1 -> program[positionOutput] = program[positionLeft] + program[positionRight]
            2 -> program[positionOutput] = program[positionLeft] * program[positionRight]
        }
        currentPosition += 4
    }
}

