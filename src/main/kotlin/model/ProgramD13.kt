package model

import java.util.LinkedList
import kotlin.math.sign

class ProgramD13(private val instructions: MutableList<Long>) {

    var blockCount = 0
    var score = 0
    var paddleX = -1
    var ballX = -1

    fun run() {
        runSuspending()
    }

    private fun runSuspending() {
        var instructionPointer = 0
        var relativeBase = 0
        val inputs = LinkedList<Int>()
        val outputs = mutableListOf<Int>()

        do {
            val nextOp = InstructionD13(instructionPointer, instructions)

            if (nextOp is InstructionD13.Input) {
                inputs.addLast((ballX - paddleX).sign)
            }

            val result = nextOp.execute(instructionPointer, relativeBase, instructions, inputs, outputs)
            instructionPointer += result.first
            relativeBase += result.second

            if (outputs.isNotEmpty() && outputs.size % 3 == 0) {
                val x = outputs[0]
                val y = outputs[1]
                val tileId = outputs[2]

                if (x == -1 && y == 0) {
                    score = tileId
                } else {
                    when (tileId) {
                        3 -> paddleX = x
                        4 -> ballX = x
                        2 -> blockCount++
                    }
                }

                outputs.clear()
            }

        } while (nextOp !is InstructionD13.Halt)
    }

}
