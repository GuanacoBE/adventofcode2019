fun main(args: Array<String>) {
    var numberOfPassword = 0

    for (number in 152085..670283) {
        val numberStr = number.toString()
        var validTwoAdjacent = false
        var validNeverDecrease = true
        for (i in 0 until (numberStr.length - 1)) {
            if (!validTwoAdjacent && numberStr[i] == numberStr[i + 1]) {
                validTwoAdjacent = true
            }
            if (numberStr[i].digitToInt() > numberStr[i + 1].digitToInt()) {
                validNeverDecrease = false
            }
        }
        if (validTwoAdjacent && validNeverDecrease) {
            numberOfPassword++
        }
    }

    println(numberOfPassword) // 1764

}