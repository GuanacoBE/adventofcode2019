import kotlinx.coroutines.channels.Channel
import kotlin.math.abs
import kotlin.math.atan2

data class Point2D(val x: Int, val y: Int) {
    fun up(): Point2D = copy(y = y + 1)
    fun down(): Point2D = copy(y = y - 1)
    fun left(): Point2D = copy(x = x - 1)
    fun right(): Point2D = copy(x = x + 1)

    fun distanceTo(other: Point2D): Int = abs(x - other.x) + abs(y - other.y)

    fun angleTo(other: Point2D): Double {
        val d = Math.toDegrees(
            atan2(
                (other.y - y).toDouble(),
                (other.x - x).toDouble()
            )
        ) + 90
        return if (d < 0) d + 360 else d
    }

    companion object {
        val ORIGIN = Point2D(0, 0)
        val leftToRightOrder: Comparator<Point2D> = Comparator { p1, p2 ->
            when {
                p1.y != p2.y -> p1.y - p2.y
                else -> p1.x - p2.x
            }
        }
    }
}

fun getLinesOfFile(fileName: String): MutableList<String> {
    return getInput(fileName)
        .split("\n")
        .toMutableList()
}

fun getInput(fileName: String): String = {}.javaClass.getResource(fileName).readText(Charsets.UTF_8)

fun <T> List<T>.permutations(): List<List<T>> {
    return if (this.size <= 1) {
        listOf(this)
    } else {
        val elementToInsert = first()
        drop(1).permutations().flatMap { permutation ->
            (0..permutation.size).map { i ->
                permutation.toMutableList().apply { add(i, elementToInsert) }
            }
        }
    }
}

fun <T> List<T>.toChannel(capacity: Int = Channel.UNLIMITED): Channel<T> =
    Channel<T>(capacity).also { this.forEach { e -> it.trySend(e).isSuccess } }

fun printImage(image: Array<Array<Int?>>) {
    for (line in image) {
        println(line.map { if (it == 0) " " else it }.joinToString(separator = ""))
    }
}

fun gcd(a: Long, b: Long): Long {
    if (b == 0L) return a
    return gcd(b, a % b)
}

fun lcm(a: Long, b: Long): Long {
    return a * b / gcd(a, b)
}
