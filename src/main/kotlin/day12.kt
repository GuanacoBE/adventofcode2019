import kotlin.math.abs
import kotlin.math.sign

fun main() {

    val system = System()
    system.addMoon(-9, 10, -1)
    system.addMoon(-14, -8, 14)
    system.addMoon(1, 5, 6)
    system.addMoon(-19, 7, 8)

    for (i in 0 until 1000) {
        system.timeStep()
    }

    println(system.computeTotalEnergy()) // 8538


    val systemPart2 = System()
    systemPart2.addMoon(-9, 10, -1)
    systemPart2.addMoon(-14, -8, 14)
    systemPart2.addMoon(1, 5, 6)
    systemPart2.addMoon(-19, 7, 8)

    val answer = systemPart2.findCycle()

    println(answer)// 506359021038056

}

class System {
    private val moons = mutableListOf<Moon>()

    fun addMoon(x: Int, y: Int, z: Int) {
        moons.add(Moon(x, y, z))
    }

    fun computeTotalEnergy(): Int = moons.sumOf { it.computeTotalEnergy() }

    fun timeStep() {
        // update the velocity of every moon by applying gravity
        applyGravity()

        // update the position of every moon by applying velocity
        applyVelocity()
    }

    private fun reset() = moons.forEach { it.reset() }

    fun findCycle(): Long {
        val stepX = findCycleCoordX()
        val stepY = findCycleCoordY()
        val stepZ = findCycleCoordZ()
        return lcm(stepX, lcm(stepY, stepZ))
    }

    private fun findCycleCoordX(): Long {
        reset()
        var stepCount = 0L
        while (true) {
            timeStep()
            stepCount++
            moons.find { it.x != it.originX || it.velocityX != 0 } ?: return stepCount
        }
    }

    private fun findCycleCoordY(): Long {
        reset()
        var stepCount = 0L
        while (true) {
            timeStep()
            stepCount++
            moons.find { it.y != it.originY || it.velocityY != 0 } ?: return stepCount
        }
    }

    private fun findCycleCoordZ(): Long {
        reset()
        var stepCount = 0L
        while (true) {
            timeStep()
            stepCount++
            moons.find { it.z != it.originZ || it.velocityZ != 0 } ?: return stepCount
        }
    }

    private fun applyVelocity() = moons.forEach { it.applyVelocity() }

    private fun applyGravity() {
        for (m1 in moons) {
            for (m2 in moons) {
                if (m1 == m2) {
                    continue
                }
                m1.applyGravity(m2)
            }
        }
    }

    override fun toString(): String {
        return "System(moons=\n$moons)"
    }

}

data class Moon(
    var x: Int,
    var y: Int,
    var z: Int
) {
    var velocityX = 0
    var velocityY = 0
    var velocityZ = 0

    val originX = x
    val originY = y
    val originZ = z

    fun applyVelocity() {
        x += velocityX
        y += velocityY
        z += velocityZ
    }

    fun applyGravity(other: Moon) {
        this.velocityX += (other.x - this.x).sign
        this.velocityY += (other.y - this.y).sign
        this.velocityZ += (other.z - this.z).sign
    }

    fun computeTotalEnergy(): Int = potentialEnergy() * kineticEnergy()

    fun reset() {
        x = originX
        y = originY
        z = originZ
        velocityX = 0
        velocityY = 0
        velocityZ = 0
    }

    private fun potentialEnergy(): Int = abs(x) + abs(y) + abs(z)

    private fun kineticEnergy(): Int = abs(velocityX) + abs(velocityY) + abs(velocityZ)

    override fun toString(): String {
        return "Moon(x=$x, y=$y, z=$z, velocityX=$velocityX, velocityY=$velocityY, velocityZ=$velocityZ)\n"
    }

}
