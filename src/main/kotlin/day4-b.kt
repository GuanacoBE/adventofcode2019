fun main(args: Array<String>) {
    var numberOfPassword = 0

    for (number in 152085..670283) {
        val numberStr = number.toString()
        if (checkDoubleDigit(numberStr) && checkNeverDecrease(numberStr)) {
            numberOfPassword++
        }
    }

    println(numberOfPassword) // 1196

}

private fun checkNeverDecrease(numberStr: String): Boolean {
    for (i in 0 until (numberStr.length - 1)) {
        if (numberStr[i].digitToInt() > numberStr[i + 1].digitToInt()) {
            return false
        }
    }
    return true
}

private fun checkDoubleDigit(number: String): Boolean {
    val letters: MutableMap<Char, MutableList<Int>> = mutableMapOf()
    number.onEachIndexed { index, c ->
        letters.getOrPut(c) {
            mutableListOf()
        }.add(index)
    }

    return letters.filter { entry ->
        entry.value.sort()
        entry.value.size == 2 && entry.value[0] == entry.value[1] - 1
    }.any()
}