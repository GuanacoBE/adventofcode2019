import model.ProgramD13

fun main() {

    val input = getInput("day13.txt")
        .split(",")
        .map { it.toLong() }
        .toMutableList()

    val p = ProgramD13(input)

    p.run()

    println(p.blockCount) // 320

    // Part 2

    input[0] = 2

    val part2 = ProgramD13(input)

    part2.run()

    println(part2.score) // 15156

}
