import model.ProgramD11

fun main() {

    val input = getInput("day11.txt")
        .split(",")
        .map { it.toLong() }
        .toMutableList()

    val p = ProgramD11(input)


    p.run()
    println(p.ship.size) // 2276

    val min = p.ship.keys.minWithOrNull(Point2D.leftToRightOrder)!!
    val max = p.ship.keys.maxWithOrNull(Point2D.leftToRightOrder)!!

    (min.y..max.y).forEach { y ->
        println(
            (min.x..max.x).map { x ->
                if (p.ship[Point2D(x, y)] == 0) ' ' else 1
            }.joinToString(separator = "")
        )
    }
    // CBLPJZCU

}
