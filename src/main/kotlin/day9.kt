import model.ProgramV5

fun main() {

    //val rawInput = "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99"
    val rawInput = getInput("day9.txt")
        .split(",")
        .map { it.toLong() }
        .toMutableList()

    val p = ProgramV5(rawInput, listOf(2L).toChannel())

    // A - 2932210790
    // B - 73144
    println(p.run())

}
