fun main(args: Array<String>) {

    //println(computeTotalFuelFor(100756))

    val input = {}.javaClass.getResource("day1.txt").readText(Charsets.UTF_8)
    val masses = input.split("\n")

    val result = masses
        .map { it.toInt() }
        .sumOf { computeTotalFuelFor(it) }

    println(result) // 5018888
}

fun computeTotalFuelFor(mass: Int): Int {
    val f = computeFuel(mass)
    return if (f <= 0) {
        0
    } else {
        f + computeTotalFuelFor(f)
    }
}
