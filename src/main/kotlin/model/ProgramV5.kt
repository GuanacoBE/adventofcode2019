package model

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.toList
import kotlinx.coroutines.runBlocking

class ProgramV5(private val instructions: MutableList<Long>, val input: Channel<Long>) {

    private val output: Channel<Long> = Channel(Channel.UNLIMITED)

    fun run(): List<Long> = runBlocking {
        runSuspending()
        output.toList()
    }

    private suspend fun runSuspending() {
        var instructionPointer = 0
        var relativeBase = 0

        do {
            val nextOp = InstructionV2(instructionPointer, instructions)
            val result = nextOp.execute(instructionPointer, relativeBase, instructions, input, output)
            instructionPointer += result.first
            relativeBase += result.second
        } while (nextOp !is InstructionV2.Halt)

        output.close()
    }

}
