fun main(args: Array<String>) {

    val rawInput = getLinesOfFile("day6.txt")

    var comLineNumber = -1
    var comLine = ""

    val distinctObject = mutableSetOf<String>()

    rawInput.forEachIndexed { index, line ->
        if (line.startsWith("COM)")) {
            comLineNumber = index
            comLine = line
        }
        val split = line.split(")")
        if (split[0] != "COM") {
            distinctObject.add(split[0])
        }
        if (split[1] != "COM") {
            distinctObject.add(split[1])
        }

    }
    rawInput.removeAt(comLineNumber)

    val orbits = rawInput.groupBy {
        val split = it.split(")")
        split[0]
    }.toMutableMap()

    val lineSplit = comLine.split(")")

    val children = orbits[lineSplit[1]]?.map {
        it.split(")")[0]
    }
    val comObject = OrbitObject(lineSplit[0], children)
    orbits.remove(lineSplit[0])

    addChildrenToChildren(orbits, comObject)

    val totalOrbiting = distinctObject.sumOf { countOrbiting(comObject, it) }

    println(totalOrbiting) // 314702
}

private fun countOrbiting(o: OrbitObject, element: String): Int {
    var dist = -1
    if (o.name == element) {
        return dist + 1
    }

    o.orbits.forEach {
        dist = countOrbiting(it, element)
        if (dist >= 0) {
            return dist + 1
        }
    }

    return dist
}

private fun addChildrenToChildren(orbits: MutableMap<String, List<String>>, orbitObject: OrbitObject) {
    orbitObject.orbits.forEach {
        it.addOrbit(orbits[it.name]?.map { o -> o.split(")")[1] }?.map { t -> OrbitObject(t) })
        orbits.remove(it.name)
        addChildrenToChildren(orbits, it)
    }
}

private class OrbitObject(val name: String) {
    val orbits: MutableSet<OrbitObject> = HashSet()

    constructor(name: String, childOrbit: List<String>?) : this(name) {
        addOrbit(childOrbit?.map { OrbitObject(it) })
    }

    fun addOrbit(o: List<OrbitObject>?) {
        o?.let { orbits.addAll(it) }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as OrbitObject

        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }

}
