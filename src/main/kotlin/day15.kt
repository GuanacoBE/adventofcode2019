import model.ProgramD15

fun main() {
    val input = getInput("day15.txt")
        .split(",")
        .map { it.toLong() }
        .toMutableList()

    val partOne = ProgramD15(input)

    partOne.run() // 424

    // Part 2

    val partTwo = partOne.visited.toMutableMap()
    var numberOfMinutes = 0

    while (partTwo.values.map { it.first }.any { it == "." }) {
        drawTheMap(partTwo)
        numberOfMinutes++
        partTwo.entries.filter { it.value.first == "O" }
            .flatMap { listOf(it.key.down(), it.key.up(), it.key.left(), it.key.right()) }
            .filter { partTwo.containsKey(it) && partTwo[it]!!.first == "." }
            .forEach { partTwo[it] = Pair("O", -1) }
    }

    println(numberOfMinutes)

}

fun drawTheMap(map: MutableMap<Point2D, Pair<String, Int>>) {
    print("\u001b[H\u001b[2J")
    val toDraw = map.toMutableMap()

    val minY = toDraw.keys.minOf { it.y }
    val maxY = toDraw.keys.maxOf { it.y }

    val minX = toDraw.keys.minOf { it.x }
    val maxX = toDraw.keys.maxOf { it.x }

    println("---------------------")
    (minY..maxY).forEach { y ->
        println(
            (minX..maxX).joinToString(separator = "") { x ->
                toDraw.getOrDefault(Point2D(x, y), Pair(" ", -1)).first
            }
        )
    }
    println("---------------------")
    Thread.sleep(1)
}
